/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.shapeframe;

/**
 *
 * @author A_R_T
 */
public class Rectangle extends Shape{
    
    private double width;
    private double hight;

    public Rectangle(double width, double hight) {
        super("Rectangle");
        this.width = width;
        this.hight = hight;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHight() {
        return hight;
    }

    public void setHight(double hight) {
        this.hight = hight;
    }

    
    @Override
    public double calArea() {
        return width*hight;
    }

    @Override
    public double calPerimeter() {
        return 0;
    }
    
    
    
}
